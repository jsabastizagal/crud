from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.template import loader, Context, RequestContext
from crud.models import Person

# Create your views here.


def index(request):
    people = Person.objects.all()
    t = loader.get_template('index.html')
    c = Context({'people': people})
    return HttpResponse(t.render(c))

def insert(request):
    # If this is a post request we insert the person
    if request.method == 'POST':
        p = Person(
            name=request.POST['name'],
            phone=request.POST['phone'],
            age=request.POST['age']
        )
        p.save()

    t = loader.get_template('insert.html')
    c = RequestContext(request)
    return HttpResponse(t.render(c))

def delete(request, person_id):
    p = Person.objects.get(pk=person_id)
    p.delete()
    return HttpResponseRedirect('/')

def edit(request, person_id):
    p = Person.objects.get(pk=person_id)
    if request.method == 'POST':
        p.name = request.POST['name']
        p.phone = request.POST['phone']
        p.age = request.POST['age']
        p.save()
    t = loader.get_template('insert.html')
    c = RequestContext(request, {
        'person': p
    })
    return HttpResponse(t.render(c))