from django.conf.urls import patterns, include, url
from django.contrib import admin
from crud import views

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'simple.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    #url(r'^', include('crud.urls')),    
    url(r'^$', views.index, name='index'),
    url(r'^insert/$', views.insert, name='insert'),
    url(r'^delete/(?P<person_id>\d+)$', views.delete, name='delete'),
    url(r'^edit/(?P<person_id>\d+)$', views.edit, name='edit')
)
